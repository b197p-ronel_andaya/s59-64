const Store = require("../utils/add-to-cart");
const Product = require("../models/product.model");
const Order = require("../models/order.model");
const auth = require("../auth");
let cartPool = [];

async function addProductToCart(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  const store = new Store(req, user, cartPool);
  const request = req.body.products[0];

  try {
    if (user.isAdmin) {
      return res.send(
        "This account was made for data administration only, please login to your normal account instead"
      );
    }
    //check product if active
    const product = await store.product();
    //return response if product is not active anymore
    if (product.length == 0) {
      return res.send("the product you are looking for is no longer available");
    }
    if (product[0].quantity === 0) {
      return res.send("Product out of stock. Please check again soon");
    }    
    //return response if cart request exceeds products actual quantity
    if (product[0].quantity < request.quantity) {
      return res.send(
        "Your cart will exceed stock limit, please adjust your cart quantity request"
      );
    }
    //modify a user cart if it's already existing
    cart = await store.cart();
    if (cart.length > 0 && cartPool.indexOf(...cart) > -1) {
      const cartIndex = cartPool.indexOf(...cart);

      const userCart = await cartPool[cartIndex];
      const userProduct = await userCart.products.filter(
        (obj) => obj.product_id == request.productId
      );
      index = await userCart.products.indexOf(...userProduct);

      //execute if product exists on user cart

      if (index > -1) {
        if (
          userCart.products[index].quantity + request.quantity >
          product[0].quantity
        ) {
          return res.send(
            "Your cart will exceed stock limit, please adjust your cart quantity request"
          );
        }
        const cartProduct = userCart.products[index];
        cartProduct.amount += request.price * request.quantity;
        cartProduct.quantity += request.quantity;
        userCart.total_amount += request.price * request.quantity;
        return res.send(await store.checkUser());
      }

      //execute if new product to push on user cart
      await userCart.products.push({
        product_id: request.productId,
        price: request.price,
        quantity: request.quantity,
        amount: request.quantity * request.price,
        img_path: request.imgPath,
      });
      userCart.total_amount += request.quantity * request.price;
      return res.send(await store.checkUser());
    }

    //execute add to cart for new user
    cartPool.push(await store.user());
    res.send(await store.checkUser());
  } catch (error) {
    next(error);
  }
}

async function viewCart(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  try {
    const store = new Store(req, user, cartPool);
    const cart = await store.checkUser();
    console.log(cart);
    if (cart.length === 0) {
      return res.send("Your cart is empty, please start shopping!");
    }
    res.send(cart);
  } catch (error) {
    return next(error);
  }
}

async function checkOut(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  const store = new Store(req, user, cartPool);
  try {
    
      if (user.isAdmin) {
        return res.send(
          "This account was made for data administration only, please login to your normal account instead"
        );
      }
   
    const userCart = await store.checkUser();
    const products = userCart[0].products;

    for (const product of products) {
      const storeProduct = await Product.findById(product.product_id);
      if (storeProduct.quantity === 0) {
        return res.send(
          `Other users already checked-out this item ${storeProduct.name}. Zero stocks at the moment. Please visit again for new stocks soon. Checkout will not proceed`
        );
      }
      if (storeProduct.quantity < product.quantity) {
        return res.send(
          `Other users already checked-out this item ${storeProduct.name}. Only ${storeProduct.quantity} left in the inventory.Please adjust your cart quantity to proceed with the order.`
        );
      }

      storeProduct.quantity -= product.quantity;
      if (storeProduct.quantity <= 0) {
        storeProduct.quantity = 0;
      }
      await storeProduct.save();
    }
    //execute order
    const order = new Order(...userCart);
    await order.save();
    //delete cart item
    cartPool.splice(cartPool.indexOf(...userCart), 1);
    return res.send("Order Sent!");
  } catch (error) {
    next(error);
  }
}

async function removeProductToCart(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  const store = new Store(req, user, cartPool);
  const userCart = await store.checkUser();

  if (user.isAdmin) {
    return res.send(
      "This account was made for data administration only, please login to your normal account instead"
    );
  }

  try {
    const cartProduct = userCart[0].products.filter(
      (obj) => obj.product_id == req.body.productId
    );

    if (cartProduct.length === 0) {
      return res.send("Product not found");
    }
    const productIndex = userCart[0].products.indexOf(...cartProduct);
    const productAmount = userCart[0].products[productIndex].amount;

    await userCart[0].products.splice(productIndex, 1);

    userCart[0].total_amount -= productAmount;

    if (userCart[0].products.length === 0) {
      cartPool.splice(cartPool.indexOf(userCart[0]), 1);
      return res.send("Cart Empty");
    }
    return res.send(userCart);
  } catch (error) {
    return next(error);
  }
}

async function subractQuantityToProduct(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  const store = new Store(req, user, cartPool);
  const userCart = await store.checkUser();

  if (user.isAdmin) {
    return res.send(
      "This account was made for data administration only, please login to your normal account instead"
    );
  }
  try {
    const cartProduct = userCart[0].products.filter(
      (obj) => obj.product_id == req.body.productId
    );

    if (cartProduct.length === 0) {
      return res.send("Product not found");
    }
    const productIndex = userCart[0].products.indexOf(...cartProduct);
    userCart[0].products[productIndex].quantity -= req.body.quantity;
    userCart[0].total_amount -= req.body.quantity * userCart[0].products[productIndex].price;

    if(userCart[0].products[productIndex].quantity <= 0){
      userCart[0].total_amount += Math.abs(userCart[0].products[productIndex].quantity)*userCart[0].products[productIndex].price;
      userCart[0].products.splice(productIndex,1);
      
    }

    if(userCart[0].products.length === 0){
      cartPool.splice(cartPool.indexOf(userCart[0]), 1);
      return res.send("Cart Empty");
    }

    return res.send(userCart);
  } catch (error) {
    return next(error);
  }
}

module.exports = {
  addProductToCart: addProductToCart,
  viewCart: viewCart,
  checkOut: checkOut,
  removeProductToCart: removeProductToCart,
  subractQuantityToProduct: subractQuantityToProduct,
};
