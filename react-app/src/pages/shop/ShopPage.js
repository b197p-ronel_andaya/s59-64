import React,{useEffect, useState} from "react";

import ShopCard from "../../components/shop/ShopCard";

const ShopPage = (props) => {
  const [products, setProducts] = useState([]);

  useEffect(() =>{
    async function fetchProduct() {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/all`,{
          method:"POST",
          body: JSON.stringify({
            isAdmin:false
          })
        }
      );
      const jsonReponse = await response.json();
      setProducts(jsonReponse);
     
    }
    fetchProduct();
  },[])
  return (
    <section className="shop-page-section sec-pad mx-auto">
      <div className="auto-container">
        <div className="row clearfix">
          <div className=" col-md-12 col-sm-12 content-side">
            <div className="our-shop">

              <div className="row clearfix">
            
                { products.noProduct ?  <p>No product listed. Please start adding products.</p> :

                  products.map(product => <ShopCard product = {product} key={product._id}/>)
                }
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ShopPage;
