import React, { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../../store/user-context";

const AddressPage = () => {
  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);
  const [address, setAddress] = useState(user.address);

  const setAddressHandler = (event) => {
    
    
    setAddress(event.target.value);
  };

  const updateAddressHandler = async (event) => {
    event.preventDefault();
    if(!address || address.trim() === ''){
        Swal.fire({
            title: "Update Address",
            icon: "error",
            text: "Please do not put an empty address",
          });
          return;
    }
    const token = localStorage.getItem("token");
    const response = await fetch(`http://localhost:4000/users/address`,{
        method: "POST",
        headers:{
            Authorization: `Bearer ${token}`,
            "content-type": "application/json"
        },
        body: JSON.stringify({
            userId: user.id,
            address: address
        })
    });
  
     const jsonResponse = await response.json();
     if(jsonResponse.addressUpdate){
        Swal.fire({
            title: "Address Updated",
            icon: "success",
            text: "Address updated. You will now be redirected to your order summary and ready to proceed. Happy shopping!",
          });
          navigate("/checkout");
          setUser({
            id: user.id,
            isAdmin: user.isAdmin,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            mobileNo: user.mobileNo,
            address: address
          })
          return;
     }
    
  };
  return (
    <div className="card orders-card my-5 mx-auto col-10 col-md-6">
      <div className="card-body">
        <form onSubmit={updateAddressHandler}>
          <div class="row">
            <div className="col">
              <div className="form-group">
                <label className="label" htmlFor="description">
                  SET SHIPPING ADDRESS
                </label>
                <textarea
                  className="form-control add-product-description"
                  placeholder="e.g. good for steak"
                  rows="5"
                  value={address}
                  onChange={setAddressHandler}
                />
              </div>
            </div>
          </div>
          <div className="row pt-2">
            <div className="col">
              <div className="form-group">
                <p className="cart-p">
                  <button
                    type="submit"
                    className="form-control btn btn-primary submit px-3 cart-remove-btn"
                  >
                    Update
                  </button>
                </p>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddressPage;
