import React, { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../../store/user-context";
import Product from "../../components/products/Products";
const ProductsPage = (props) => {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  console.log("you are here at product page");
  useEffect(() => {
    async function getAllProducts() {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/all`,{
          method: "POST",
          headers:{
            "content-type":"application/json"
          },
          body: JSON.stringify({
            isAdmin: true
          })
        }
      );
      const jsonResponse = await response.json();
      setProducts(jsonResponse);
    }
    getAllProducts();
  }, []);
  return (
    <>
      {!user.isAdmin ? (
        <Navigate to="/shop" />
      ) : (

        <div class="container py-5">

        <div className="row">
                  <div className="card orders-card mb-2">
                    <div className="card-body">
                      <h4 className="text-center">Product List</h4>
                      <hr className="cart-hr"/>
                      <table class="table table-borderless table-orders">
                        <thead>
                          <tr>
                            <th scope="col table-orders-header">
                              ITEMS
                            </th>
                            <th scope="col">NAME</th>
                            <th scope="col">STOCKS(KG)</th>
                            <th scope="col">AMOUNT</th>
                            <th scope="col">ACTIVE</th>
                            <th scope="col">ACTION</th>
                          </tr>
                        </thead>
                        <tbody>
                          {products.map((product) => (
                            <Product key={product.productId} product={product} />
                              
                            
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
        </div>
      )}
    </>
  );
};

export default ProductsPage;
