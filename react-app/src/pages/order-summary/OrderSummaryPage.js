import React, { useEffect, useState, useContext } from "react";
import UserContext from "../../store/user-context";
import OrderSummary from "../../components/order-summary/OrderSummary";


const OrderSummaryPage = (props) => {
  const {user} = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    async function fetchOrders() {
      const token = localStorage.getItem("token");
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/orders/`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "content-type": "application/json",
          },
        }
      );

      const jsonReponse = await response.json();
      setOrders(jsonReponse);
    }
    fetchOrders();
  }, []);

  return (      <div className="container py-5">

  <div className="row">
            <div className="card orders-card mb-2">
              <div className="card-body">
                <h4 className="text-center">Order Summary List</h4>
                <hr className="cart-hr"/>
                <table className="table table-borderless table-orders">
                  <thead>
                    <tr>
                    {user.isAdmin ? <th scope="col">ORDER ID</th>:''}
                      
                      <th scope="col">TRACKING #</th>
                      <th scope="col">ORDER DATE</th>
                      {user.isAdmin ?<><th scope="col">CONTACT #</th>
                      <th scope="col">NAME</th></> : '' }
                      
                      <th scope="col">TOTAL AMOUNT</th>
                      <th scope="col">STATUS</th>
                      <th scope="col set-admin-action">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((order) => (
                      <OrderSummary key={order._id} order={order} />
                        
                      
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
  </div>)

};

export default OrderSummaryPage;
