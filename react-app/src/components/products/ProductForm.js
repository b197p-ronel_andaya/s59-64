import React, { useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import LogoWhite from "../../assets/images/logoWhite.png";
import Swal from "sweetalert2";
import "./ProductForm.css";

const ProductForm = (props) => {
  const { productId } = useParams();
  console.log(productId);
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [quantity, setQuantity] = useState("");
  const [image, setImage] = useState(null);
  const [isActive, setIsActive] = useState(true);
  const [isCreateMode, setIsCreateMode] = useState(true);

  useEffect(() => {
    if (productId) {
      async function fetchProduct() {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/products/${productId}`
        );
        const jsonReponse = await response.json();
        setName(jsonReponse.name);
        setDescription(jsonReponse.description);
        setPrice(jsonReponse.price);
        setQuantity(jsonReponse.quantity);
        setIsCreateMode(false);
        setIsActive(jsonReponse.is_active);
      }
      fetchProduct();
    } 
  }, [productId]);

  useEffect(() => {
    document.getElementById("isActiveCheckbox").checked = isActive;
  }, [isActive]);

  const setNameHandler = (event) => {
    setName(event.target.value);
  };
  const setDescriptionHandler = (event) => {
    setDescription(event.target.value);
  };
  const setPriceHandler = (event) => {
    setPrice(event.target.value);
  };
  const setQuantityHandler = (event) => {
    setQuantity(event.target.value);
  };
  const setImageHandler = (event) => {
    setImage(event.target.files[0]);
  };
  const setIsActiveHandler = (event) => {
    setIsActive(!isActive);
  };

  const createProductHandler = async (event) => {
    event.preventDefault();
    console.log(isActive);

    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", description);
    formData.append("price", price);
    formData.append("quantity", quantity);
    formData.append("image", image);
    console.log(formData);

    const token = localStorage.getItem("token");
    let response = null;
    if (isCreateMode) {
      response = await fetch(`${process.env.REACT_APP_API_URL}/products`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      });
    } else {
      formData.append("productId", productId);
      formData.append("isActive", isActive);

      response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/update`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${token}`,
          },
          body: formData,
        }
      );
    }

    const jsonReponse = await response.json();

    console.log(jsonReponse);
    if (jsonReponse) {
      Swal.fire({
        title: "Product created!",
        icon: "success",
        text: "Product successfully created!",
      });
      setName("");
      setDescription("");
      setPrice("");
      setQuantity("");
      setImage(null);
      navigate("/products");
    }
  };

  return (
    <div className="container mt-5 mb-5">
      <div className="row justify-content-center">
        <div className="col-md-12 col-lg-10">
          <div className="wrap d-md-flex">
            <div className="text-wrap p-4 p-lg-5 text-center d-flex flex-col align-items-center order-md-last">
              <div className="text w-100">
                <Link to="/">
                  <img
                    src={LogoWhite}
                    alt=""
                    title=""
                    className="w-50 pb-3"
                  ></img>
                </Link>
                <h2>{!isCreateMode ? "Update Product" : "Add Product"}</h2>
                <p>Go to Products</p>
                <Link
                  to="/products"
                  className="btn btn-white btn-outline-white"
                >
                  Products
                </Link>
              </div>
            </div>
            <div className="login-wrap p-4 p-lg-5">
              <div className="d-flex">
                <div className="w-100">
                  <h3 className="mb-4">
                    {!isCreateMode ? "Update Product" : "Add Product"}
                  </h3>
                </div>
              </div>
              <form
                className="add-product-form"
                onSubmit={createProductHandler}
              >
                <div className="form-group mb-3">
                  <label className="label" htmlFor="name">
                    Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Tenderloin"
                    required
                    value={name}
                    onChange={setNameHandler}
                  />
                </div>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="description">
                    Description
                  </label>
                  <textarea
                    className="form-control add-product-description"
                    placeholder="e.g. good for steak"
                    rows="5"
                    value={description}
                    onChange={setDescriptionHandler}
                  />
                </div>
                <div className="form-group mb-3">
                  <div className="row">
                    <div className="col">
                      <label className="label" htmlFor="price">
                        Price(in PHP)
                      </label>
                      <input
                        type="number"
                        className="form-control"
                        placeholder="300"
                        required
                        value={price}
                        onChange={setPriceHandler}
                      />
                    </div>
                    <div className="col">
                      <label className="label" htmlFor="quantity">
                        Quantity(in KG)
                      </label>
                      <input
                        type="number"
                        className="form-control"
                        placeholder="5"
                        required
                        value={quantity}
                        onChange={setQuantityHandler}
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="image">
                    Image
                  </label>
                  <input
                    type="file"
                    accept="image/*"
                    // className="form-control"
                    placeholder="Tenderloin"
                    onChange={setImageHandler}
                  />
                </div>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    id="isActiveCheckbox"
                    type="checkbox"
                    onChange={setIsActiveHandler}
                    value={isActive}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    Active
                  </label>
                </div>

                <div className="form-group pt-3 mb-0">
                  <button
                    type="submit"
                    className="form-control btn btn-primary submit px-3"
                  >
                    {!isCreateMode ? "Update" : "Create"}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductForm;
