import React, { useState, useEffect, useContext } from "react";
import {Link} from 'react-router-dom';
import UserContext from "../../store/user-context";
import DeliveryIcon from '../../assets/images/icons/delivery.png';
import SwitchIcon from '../../assets/images/icons/switch.png';

const OrderSummary = (props) => {
  const [name, setName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [trackingNumber, setTrackingNumber] = useState("");
  const { user } = useContext(UserContext);
  const [delivered, setDelivered] = useState(props.order.delivered);
  

  const deliveryHandler = async () =>{
    const token = localStorage.getItem("token");
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/orders/delivery`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "content-type": "application/json",
        },
        body: JSON.stringify({
          orderId: props.order._id,
          delivered: true
        }),
      }
    );

    const jsonReponse = await response.json();
    if(jsonReponse.wasDelivered){
        setDelivered(true);
    }
  }

  const switchHandler = async () =>{
    const token = localStorage.getItem("token");
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/orders/delivery`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "content-type": "application/json",
        },
        body: JSON.stringify({
          orderId: props.order._id,
          delivered: false
        }),
      }
    );

    const jsonReponse = await response.json();
    if(jsonReponse.wasDelivered){
        setDelivered(false);
    }
  }

  useEffect(() => {
    async function fetchUser() {
      const token = localStorage.getItem("token");
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/details`,
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
            "content-type": "application/json",
          },
          body: JSON.stringify({
            userId: props.order.user_id,
          }),
        }
      );

      const jsonReponse = await response.json();
      setName(`${jsonReponse.firstName} ${jsonReponse.lastName}`);
      setMobileNo(jsonReponse.mobileNo);
    }
    fetchUser();

    for (const product of props.order.products) {
      setTrackingNumber(product.tracking_number);
      return;
    }
  }, [props.order.products, props.order.user_id]);


  return (
    <tr>
      {user.isAdmin ? <td>{props.order._id}</td> : ""}

      <td>{trackingNumber}</td>
      <td>{new Date(props.order.purchased_on).toLocaleDateString()}</td>

      {user.isAdmin ? (
        <>
          <td>{mobileNo}</td>
          <td>{name}</td>
        </>
      ) : (
        ""
      )}

      <td>{props.order.total_amount.toFixed(2)} PHP</td>
      {delivered && user.isAdmin? 
        <td className="text-success">Delivered <img src={SwitchIcon} alt="" className="active-product" onClick={switchHandler}/></td>:
        <td
        className={props.order.is_cancelled ? "text-warning" : "text-success"}
      >
    
        {props.order.is_cancelled? "Cancelled " : ( delivered ? 'Delivered':'For Delivery')}
        {!props.order.is_cancelled && user.isAdmin?        
        <img src={DeliveryIcon} alt="" className="active-product" onClick={deliveryHandler}/>: ''
        }
      </td>
      }
      
      
      <td><Link to={`/orders/view/${props.order._id}`}>View</Link></td>
    </tr>
  );
};

export default OrderSummary;
