import React from "react";
import {Link} from "react-router-dom";
import Menu from "../menu/Menu";


import LogoWhite from '../../assets/images/logoWhite.png'
const MobileMenu = (props) => {
  const hideMobileMenuHandler = () => {
    document.body.classList.remove("mobile-menu-visible");
  };
  return (
    <div className="mobile-menu" onClick={hideMobileMenuHandler}>
      <div className="menu-backdrop"></div>
      <div className="close-btn">
        <i className="fas fa-times"></i>
      </div>

      <nav className="menu-box mCustomScrollbar _mCS_1">
        <div className="nav-logo">
          <Link to="/">
            <img src={LogoWhite} alt="" title=""></img>
          </Link>
        </div>
        <div className="menu-outer">
          <Menu />
        </div>
        <div className="contact-info">
          <h4>Contact Info</h4>
          <ul>
            <li>Blk 12 Lot 24, Sacot, Bamban, Tarlac, 2317</li>
            <li>
              <a href="tel:639279970588">+63 9279970588</a>
            </li>
            <li>
              <a href="mailto:steakseafoodandmore@gmail.com">steakseafoodandmore@gmail.com</a>
            </li>
          </ul>
        </div>
        <div className="social-links">
          <ul className="clearfix d-flex justify-content-center">
            <li>
              <a href="https://www.facebook.com/Steak-Seafood-N-More-100548602462066"  target="_blank" rel="noreferrer">
                <span className="fab fa-facebook-square"></span>
              </a>
            </li>
            <li>
              <a href="https://www.instagram.com/raphaels_steakhouse/" target="_blank" rel="noreferrer">
                <span className="fab fa-instagram"></span>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default MobileMenu;
