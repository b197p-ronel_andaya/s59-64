import React, { useState, useContext} from "react";
import { Link } from "react-router-dom";
import { Navigate } from "react-router-dom";
import UserContext from "../../store/user-context";
import Swal from "sweetalert2";
import "./Login.css";
import LogoWhite from "../../assets/images/logoWhite.png";
const Login = () => {


  const {user, setUser} = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const loginHandler = async (event) => {
    event.preventDefault();
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/users/login`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      }
    );

    const jsonResponse = await response.json();
    console.log(jsonResponse.access);
    if (typeof jsonResponse.access !== "undefined") {
      localStorage.setItem("token", jsonResponse.access);
      retrieveUserDetails(jsonResponse.access);

      Swal.fire({
        title: "Login Successful",
        icon: "success",
        text: "Meat-Up your Cart!",
      });
    } else {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Please check your login details and try again!",
      });
    }
    setEmail("");
    setPassword("");
  };

  const retrieveUserDetails = async (token) => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/users/details`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    const jsonResponse = await response.json();
    console.log('user');
    console.log(jsonResponse);

    
   setUser({
    id: jsonResponse._id,
    isAdmin: jsonResponse.is_admin,
    firstName: jsonResponse.firstName,
    lastName: jsonResponse.lastName,
    email: jsonResponse.email,
    mobileNo: jsonResponse.mobileNo,
    address: jsonResponse.address
   })
    
  };

  const setEmailHandler = (event) => {
    setEmail(event.target.value);
  };

  const setPasswordHandler = (event) => {
    setPassword(event.target.value);
  };

  return (
    (user.id !== null) ?
			<Navigate to="/shop"/> :
    <div className="container mt-5 mb-5">
      <div className="row justify-content-center">
        <div className="col-md-12 col-lg-10">
          <div className="wrap d-md-flex">
            <div className="text-wrap p-4 p-lg-5 text-center d-flex flex-col align-items-center order-md-last">
              <div className="text w-100">
                <Link to="/">
                  <img
                    src={LogoWhite}
                    alt=""
                    title=""
                    className="w-50 pb-3"
                  ></img>
                </Link>
                <h2>Welcome to login</h2>
                <p>Don't have an account?</p>
                <Link
                  to="/register"
                  className="btn btn-white btn-outline-white"
                >
                  Register
                </Link>
              </div>
            </div>
            <div className="login-wrap p-4 p-lg-5">
              <div className="d-flex">
                <div className="w-100">
                  <h3 className="mb-4">Sign In</h3>
                </div>
              </div>
              <form className="signin-form" onSubmit={loginHandler}>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="email">
                    Email
                  </label>
                  <input
                    id="email"
                    type="text"
                    className="form-control"
                    placeholder="juandelacruz@email.com"
                    value={email}
                    onChange={setEmailHandler}
                    required
                  />
                </div>
                <div className="form-group mb-3">
                  <label className="label" htmlFor="password">
                    Password
                  </label>
                  <input
                    id="password"
                    type="password"
                    className="form-control"
                    placeholder="password"
                    value={password}
                    onChange={setPasswordHandler}
                    required
                  />
                </div>
                <div className="form-group pt-3 mb-0">
                  <button
                    type="submit"
                    className="form-control btn btn-primary submit px-3"
                  >
                    Sign In
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Login;
